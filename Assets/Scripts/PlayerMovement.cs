﻿//Tristan Burger
//8-4-2-16
// Used for player movement
using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    [Header("Movement")]
    [Tooltip("Speed multiplier for Player movement")]
    [Range(1,15)]
    public float moveSpeed;

    [Header("Jumping")]
    [SerializeField]
    [Range(1, 15)]
    float jumpSpeed = 5;

    [SerializeField]
    int numberOfJumps = 1;
    int timesCanJump;

    void Start ()
    {
        timesCanJump = numberOfJumps;
    }

    void Update ()
    {
        UpdateMovement();
    }

    void UpdateMovement ()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += transform.forward * moveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position -= transform.forward * moveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.position -= transform.right * moveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += transform.right * moveSpeed * Time.deltaTime;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (timesCanJump > 0)
            {
                GetComponent<Rigidbody>().AddForce(Vector3.up * jumpSpeed * 100);
                timesCanJump--;
            }
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            Debug.Log(GenericTools.FlipCoin());
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.collider.tag == "Ground")
        {
            timesCanJump = numberOfJumps;
        }
    }

    void OnTriggerStay (Collider col)
    {
        if(col.tag == "Enemy" && Input.GetButtonDown("Fire1"))
        {
            col.GetComponent<EnemyManagement>().TakeDamage();
        }
    }
}
