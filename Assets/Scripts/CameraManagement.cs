﻿//Tristan Burger
//8-4-2-16
// Used for player movement
using UnityEngine;
using System.Collections;

public class CameraManagement : MonoBehaviour {

    GameObject player;

    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update ()
    {
        transform.position = player.transform.position;
    }
}
