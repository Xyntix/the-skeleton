﻿//Tristan Burger
//8-4-2-16
// Used for player movement
using UnityEngine;
using System.Collections;

public class EnemyManagement : MonoBehaviour {

    public int maxHealth;
    int health;

    void OnEnable ()
    {
        health = maxHealth;
        GetComponent<AIPath>().target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public void TakeDamage (int damage = 1)
    {
        health -= damage;
        if(health <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
